﻿namespace Проєкт1
{
    partial class AddGroupmateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.FullNameTextBox = new System.Windows.Forms.TextBox();
            this.ProgrammingTextBox = new System.Windows.Forms.TextBox();
            this.EnglishTextBox = new System.Windows.Forms.TextBox();
            this.DiscreteTextBox = new System.Windows.Forms.TextBox();
            this.MathAnalismTextBox = new System.Windows.Forms.TextBox();
            this.WebTextBox = new System.Windows.Forms.TextBox();
            this.AlgebraTextBox = new System.Windows.Forms.TextBox();
            this.ITManagementTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "ПІБ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Програмування";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Веб-програмування";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 221);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 20);
            this.label5.TabIndex = 13;
            this.label5.Text = "Алгебра";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 277);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(175, 20);
            this.label6.TabIndex = 14;
            this.label6.Text = "Математичний аналіз";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 329);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(188, 20);
            this.label7.TabIndex = 15;
            this.label7.Text = "Дискретна математика";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 380);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(134, 20);
            this.label8.TabIndex = 17;
            this.label8.Text = "Англійська мова";
            // 
            // FullNameTextBox
            // 
            this.FullNameTextBox.Location = new System.Drawing.Point(274, 22);
            this.FullNameTextBox.Name = "FullNameTextBox";
            this.FullNameTextBox.Size = new System.Drawing.Size(219, 26);
            this.FullNameTextBox.TabIndex = 18;
            // 
            // ProgrammingTextBox
            // 
            this.ProgrammingTextBox.Location = new System.Drawing.Point(274, 75);
            this.ProgrammingTextBox.Name = "ProgrammingTextBox";
            this.ProgrammingTextBox.Size = new System.Drawing.Size(219, 26);
            this.ProgrammingTextBox.TabIndex = 19;
            // 
            // EnglishTextBox
            // 
            this.EnglishTextBox.Location = new System.Drawing.Point(274, 374);
            this.EnglishTextBox.Name = "EnglishTextBox";
            this.EnglishTextBox.Size = new System.Drawing.Size(219, 26);
            this.EnglishTextBox.TabIndex = 20;
            // 
            // DiscreteTextBox
            // 
            this.DiscreteTextBox.Location = new System.Drawing.Point(274, 326);
            this.DiscreteTextBox.Name = "DiscreteTextBox";
            this.DiscreteTextBox.Size = new System.Drawing.Size(219, 26);
            this.DiscreteTextBox.TabIndex = 21;
            // 
            // MathAnalismTextBox
            // 
            this.MathAnalismTextBox.Location = new System.Drawing.Point(274, 274);
            this.MathAnalismTextBox.Name = "MathAnalismTextBox";
            this.MathAnalismTextBox.Size = new System.Drawing.Size(219, 26);
            this.MathAnalismTextBox.TabIndex = 22;
            // 
            // WebTextBox
            // 
            this.WebTextBox.Location = new System.Drawing.Point(274, 125);
            this.WebTextBox.Name = "WebTextBox";
            this.WebTextBox.Size = new System.Drawing.Size(219, 26);
            this.WebTextBox.TabIndex = 23;
            // 
            // AlgebraTextBox
            // 
            this.AlgebraTextBox.Location = new System.Drawing.Point(274, 221);
            this.AlgebraTextBox.Name = "AlgebraTextBox";
            this.AlgebraTextBox.Size = new System.Drawing.Size(219, 26);
            this.AlgebraTextBox.TabIndex = 24;
            // 
            // ITManagementTextBox
            // 
            this.ITManagementTextBox.Location = new System.Drawing.Point(274, 174);
            this.ITManagementTextBox.Name = "ITManagementTextBox";
            this.ITManagementTextBox.Size = new System.Drawing.Size(219, 26);
            this.ITManagementTextBox.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(199, 20);
            this.label4.TabIndex = 26;
            this.label4.Text = "Управління ІТ проектами";
            // 
            // addButton
            // 
            this.addButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.addButton.Location = new System.Drawing.Point(567, 353);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(174, 47);
            this.addButton.TabIndex = 27;
            this.addButton.Text = "Додати";
            this.addButton.UseVisualStyleBackColor = true;
            // 
            // AddGroupmateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ITManagementTextBox);
            this.Controls.Add(this.AlgebraTextBox);
            this.Controls.Add(this.WebTextBox);
            this.Controls.Add(this.MathAnalismTextBox);
            this.Controls.Add(this.DiscreteTextBox);
            this.Controls.Add(this.EnglishTextBox);
            this.Controls.Add(this.ProgrammingTextBox);
            this.Controls.Add(this.FullNameTextBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddGroupmateForm";
            this.Text = "AddGroupmateForm";
            this.Load += new System.EventHandler(this.AddGroupmateForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox FullNameTextBox;
        public System.Windows.Forms.TextBox ProgrammingTextBox;
        public System.Windows.Forms.TextBox EnglishTextBox;
        public System.Windows.Forms.TextBox DiscreteTextBox;
        public System.Windows.Forms.TextBox MathAnalismTextBox;
        public System.Windows.Forms.TextBox WebTextBox;
        public System.Windows.Forms.TextBox AlgebraTextBox;
        public System.Windows.Forms.TextBox ITManagementTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button addButton;
    }
}