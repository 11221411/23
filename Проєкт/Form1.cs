﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Проєкт1
{
    public partial class Form1 : Form
    {
        List<Groupmates> GroupmatesList;
        BindingSource BindSource;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GroupmatesList = new List<Groupmates>();

           GroupmatesList.Add(new Groupmates("Новікова Р.В", 90, 90, 100, 93, 80, 92, 95));

            BindSource = new BindingSource();
            BindSource.DataSource = GroupmatesList;
            GroupmatesInfo.DataSource = BindSource;

            GroupmatesInfo.Columns["Full Name"].HeaderText = "ПІБ";
            GroupmatesInfo.Columns["Programming"].HeaderText = "Програмування";
            GroupmatesInfo.Columns["WEB"].HeaderText = "Веб-програмування";
            GroupmatesInfo.Columns["IT Managment"].HeaderText = "Управління ІТ проєктами";
            GroupmatesInfo.Columns["Algebra"].HeaderText = "Алгебра";
            GroupmatesInfo.Columns["Math Analism"].HeaderText = "Матем. аналіз";
            GroupmatesInfo.Columns["Discrete"].HeaderText = "Дискретна математика";
            GroupmatesInfo.Columns["English"].HeaderText = "Англійська мова";
            GroupmatesInfo.Columns["Average"].HeaderText = "Середній бал";
        }

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, GroupmatesList);
                fs.Close();
            }
        }

        private void завантажитиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(openFileDialog.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                BinaryFormatter bf = new BinaryFormatter();
                BindSource.Clear();
                foreach (Groupmates b in (List<Groupmates>)bf.Deserialize(fs))
                    BindSource.Add(b);
                fs.Close();
            }
        }

        List<Groupmates> GroupmatesListFiltered;
        BindingSource BindSourceFiltered;

        private void button1_Click(object sender, EventArgs e)
        {
            string FullName = searchFullName.Text;
            GroupmatesListFiltered = GroupmatesList.FindAll(groupmate => groupmate.FullName.Contains(FullName));

            BindSourceFiltered = new BindingSource();
            BindSourceFiltered.DataSource = GroupmatesListFiltered;

            searchResultGrid.DataSource = BindSourceFiltered;
        }

        private void вихідToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void додатиСтудентаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var addForm = new AddGroupmateForm();
            if (addForm.ShowDialog() == DialogResult.OK)
            {
                string newFullName = addForm.FullNameTextBox.Text;
                int newProgramming = Convert.ToInt32(addForm.ProgrammingTextBox.Text);
                int newWeb = Convert.ToInt32(addForm.WebTextBox.Text);
                int newITManagement = Convert.ToInt32(addForm.ITManagementTextBox.Text);
                int newAlgebra = Convert.ToInt32(addForm.AlgebraTextBox.Text);
                int newMathAnalism = Convert.ToInt32(addForm.MathAnalismTextBox.Text);
                int newDiscrete = Convert.ToInt32(addForm.DiscreteTextBox.Text);
                int newEnglish = Convert.ToInt32(addForm.EnglishTextBox.Text);


                Groupmates groupmate = new Groupmates(newFullName, newProgramming, newWeb, newITManagement, newAlgebra, newMathAnalism, newDiscrete, newEnglish);
                BindSource.Add(groupmate);
            }
        }

        private void визначитиСереднійБалToolStripMenuItem_Click(object sender, EventArgs e, int rowIndex)
        {
            
        }

        private void визначитиСереднійБалToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double[,] data = new double[GroupmatesInfo.RowCount,GroupmatesInfo.ColumnCount-1] ;
            DataGridViewRow row = GroupmatesInfo.Rows[0];
            for(int i =0; i<GroupmatesInfo.RowCount; i++)
            {
                for (int j = 0; j<GroupmatesInfo.ColumnCount-1; j++)
                {
                    data[i, j] = Convert.ToDouble(GroupmatesInfo[j+1, i].Value);
                }
            }
            int rowCount = data.GetLength(0);
            int columnCount = data.GetLength(1)-1;

            int targetColumnIndex = 8;
            for (int i = 0; i<GroupmatesInfo.RowCount; i++)
            {
                double sum = 0;
                for (int j = 0; j<GroupmatesInfo.ColumnCount-1; j++)
                {
                    double value = data[i, j];
                    sum += value;
                }
                double average = sum / columnCount;
                GroupmatesInfo[targetColumnIndex, i].Value = average;
            }
            //row.Cells[targetColumnIndex].Value = average;
            //MessageBox.Show(average.ToString());
        }

        private void експортВExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Excel.saveGrid(GroupmatesInfo, saveFileDialog.FileName, "Export_data");
            }
        }
    }
}

