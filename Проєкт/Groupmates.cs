﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Проєкт1
{
    [Serializable]
    internal class Groupmates
    {
        public string FullName { get; set; }
        public int Programming { get; set; }
        public int Web { get; set; }
        public int ITManagement { get; set; }
        public int Algebra { get; set; }
        public int MathAnalism { get; set; }
        public int Discrete { get; set; }
        public int English { get; set; }
        public double Average { get; set; }
        public Groupmates(string _fullName, int _programming, int _web, int _itManagment, int _algebra, int _mathAnalism, int _discrete, int _english)
        {
            FullName= _fullName;
            Programming= _programming;
            Web= _web;
            ITManagement= _itManagment;
            Algebra= _algebra;
            MathAnalism= _mathAnalism;
            Discrete= _discrete;
            English= _english;
        }
    }
}
