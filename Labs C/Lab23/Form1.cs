﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System. IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Лабораторна_23
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.RowCount = (int) numericUpDownRows.Value;
            dataGridView1.ColumnCount = (int)numericUpDownColumns.Value;
        }

        private void numericUpDownRows_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.RowCount = (int)numericUpDownRows.Value;
        }

        private void numericUpDownColumns_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = (int)numericUpDownColumns.Value;
        }

        private void згенеруватиМасивToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Matrix.FillGridWithRandomNumbers(dataGridView1);
        }

        private void завантажитиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Matrix.ReadGridDataFromFile(openFileDialog1.FileName, dataGridView1);
                numericUpDownRows.Value = dataGridView1.RowCount;
                numericUpDownColumns.Value = dataGridView1.ColumnCount;

            }
        }

        private void визначитиНайменшийЕлементСередДодатніхToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double[,] n = Matrix.ReadMatrixToGrid(dataGridView1);
            double min = 1000;
            foreach (var item in n)
            {
                if (item > 0 && item<min)
                {
                    min = item;
                }
            }
            MessageBox.Show(min.ToString());

        }

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() ==DialogResult.OK)
            {
                Matrix.SaveGridToFile(saveFileDialog1.FileName, dataGridView1);
            }
        }
    }
}
